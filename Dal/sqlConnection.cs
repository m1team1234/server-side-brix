﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dal
{
    public static class sqlConnection
    {

        /// <summary>
        /// This function invokes a procedure in SQL
        /// </summary>
        /// <param name="procedureName">Please submit the name in full, including added characters.</param>
        /// <returns>DataSet with all the data that accepted from the database.</returns>
        public static DataSet callProcedure(string connectionString, string procedureName,  List<SqlParameter> list)
        {
            try
            {
                //sql connection object
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    string spName = procedureName;
                    SqlCommand cmd = new SqlCommand(spName,cn);
                    if(list.Count>0)//I have parameters
                    foreach (var item in list)
                    {
                        cmd.Parameters.Add(item);
                    }
                    cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet("res");
                    da.Fill(ds);
                    cmd.Parameters.Clear();
                    cn.Close();
                    return ds;

                }
            }
            catch (Exception ex)
           
            {
                //display error message
                DataSet ds = new DataSet("res");
                Console.WriteLine(ex.Message);
                return ds;
            }
        }
        public static DataSet callProcedure(string connectionString, string procedureName, SqlParameter param)
        {
            List<SqlParameter> parameters = new List<SqlParameter>() { param };
            return callProcedure(connectionString, procedureName, parameters);               
        }


        public static DataSet callProcedure(string connectionString, string procedureName)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return callProcedure(connectionString, procedureName, parameters);
        }
    }
}

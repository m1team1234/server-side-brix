﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Idal;

namespace Dal
{
    [Implement(typeof(IwhiteList))]

    public class whiteList : IwhiteList
    {
        public DataSet AddIp(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString, @"dbo.[AddIp]", list);
        }

        public DataSet DeleteIp(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString, @"dbo.[DeleteIp]", list[0]);
        }

        public DataSet EditDescription(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString, @"dbo.[EditDescription]", list);
        }

        public DataSet GetAllList(string connectionString)
        {
            return sqlConnection.callProcedure(connectionString, @"dbo.[getAllListIp]");
        }
        public DataSet IsIPExist(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString, @"dbo.[IsIPExist]",list);
        }
    }
}

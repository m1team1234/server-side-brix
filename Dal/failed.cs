﻿using Idal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dal
{
    [Implement(typeof(Ifailed))]

    public class failed : Ifailed
    {
        public DataSet AddFailedIp(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString,@"dbo.[AddFailedIp]", list);
        }

        public DataSet DeleteFailedIp(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString,@"dbo.[DeleteFailedIp]", list);
        }

        public DataSet GetAllFailed(string connectionString)
        {
            return sqlConnection.callProcedure(connectionString,@"dbo.[GetAllFailed]");
        }

        public DataSet IsfailedIpIPExist(string connectionString, List<SqlParameter> list)
        {
            return sqlConnection.callProcedure(connectionString,@"dbo.[IsfailedIpIPExist]", list);
        }
    }
}

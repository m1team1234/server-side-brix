﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace CheckSendMail
{
    public class MailMessageDTO
    {
        public string from { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public List<string> listTo { get; set; }
        public List<string> listAttchment { get; set; }

    }
}

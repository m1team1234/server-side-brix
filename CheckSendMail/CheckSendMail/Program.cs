﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using MailMessage;

namespace CheckSendMail
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static async Task Main()
        {
            SendRequest mail = new SendRequest();
            mail.from = "brix.dev.m1@gmail.com";
            mail.subject = "try mail sending";
            mail.body = "hello to all!!!!!!!!!!!!!!!!!";
            mail.listTo = new List<string>() { "562tamar@gmail.com", "chanigrozo@gmail.com", "esti30248@gmail.com","bt3232675@gmail.com" };
            try
            {
                var myContent = JsonConvert.SerializeObject(mail);
                var buffer = Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                
                var result = client.PostAsync("https://localhost:44398/api/Send/send", byteContent).Result;

                result.EnsureSuccessStatusCode();
                string responseBody = await result.Content.ReadAsStringAsync();
                SendResponse response = new SendResponse();
                response.IsSent = responseBody;
                Console.WriteLine(response.IsSent);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }


        }
    }
}


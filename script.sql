USE [brix]
GO
/****** Object:  Table [dbo].[failed]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[failed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[failedIp] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[whitelist]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[whitelist](
	[userIp] [varchar](100) NOT NULL,
	[userDescription] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[userIp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[AddFailedIp]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AddFailedIp] @failedIp varchar(100)
as
insert into failed(failedIp)
values (@failedIp)
select * from failed
where failedIp = @failedIp
GO
/****** Object:  StoredProcedure [dbo].[AddIp]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AddIp] @userIp varchar(100),@userDescription varchar(1000)
as
insert into whitelist(userIp,userDescription)
values (@userIP,@userDescription)
select * from whitelist
where userIp=@userIp
GO
/****** Object:  StoredProcedure [dbo].[DeleteFailedIp]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DeleteFailedIp] @failedIp varchar(100)
as
delete from failed  
where  failedIp = @failedIp
select @@ROWCOUNT as ii

GO
/****** Object:  StoredProcedure [dbo].[DeleteIp]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DeleteIp] @userIp varchar(100)
as
delete from whitelist  
where  userIp = @userIp
select @@ROWCOUNT as ii
GO
/****** Object:  StoredProcedure [dbo].[EditDescription]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[EditDescription]  @userIp varchar(100),@userDescription varchar(1000)
as
UPDATE whitelist
SET userDescription = @userDescription
where userIp=@userIp  
select * from whitelist


GO
/****** Object:  StoredProcedure [dbo].[getAllFailed]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getAllFailed]
as
select * from failed
GO
/****** Object:  StoredProcedure [dbo].[getAllListIp]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getAllListIp]
as
select * from whitelist
GO
/****** Object:  StoredProcedure [dbo].[IsfailedIpIPExist]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[IsfailedIpIPExist] @failedIp varchar(100)
as
select * from failed
where failedIp=@failedIp
GO
/****** Object:  StoredProcedure [dbo].[IsIPExist]    Script Date: 31/03/2020 14:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[IsIPExist] @userIp varchar(1000),@userDescription varchar(100)=''
as
select * from whitelist
where userIp=@userIp
GO

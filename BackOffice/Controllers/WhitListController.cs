﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DTO;
using Ibll;
using Idal;
using cenvert;
using Microsoft.AspNetCore.Mvc;
using BackOffice;

namespace backOfficeApi.Controllers
{
    //this controller handeled the white list actions.
    [ApiController]
    [Route("api/[controller]")]
    public class WhiteListController : ControllerBase
    {
        
        ///<summary>return all the ip's from the database.</summary>
        [HttpGet]
        [Route("{action}")]
        public List<whitelistDTO> GetAllList()        
        {
            //create an instance of class that implements IwhiteList via the factory.
            IwhiteList whitelist = factory.factorycs.resolve<IwhiteList>();
            var dataSet = whitelist.GetAllList(connectionString.connect);
            var res = convert<whitelistDTO>.convertDBsetToT(dataSet);
            return res;
        }
        // GET: api/WhitList/5
        /// <summary>
        /// the requested ip if exist in the white list,else return an empty list
        /// </summary>
        /// <param name="ip"></param>
        [HttpGet]
        [Route("{action}")]       
        public List<whitelistDTO> GetByIP([FromBody] List<string> ip)
        {
            List<SqlParameter> list = convert<string>.convertTypeToSqlParameter(ip);
            Idal.IwhiteList whitrlist = factory.factorycs.resolve<IwhiteList>();
            var dataSet = whitrlist.IsIPExist(connectionString.connect, list);
            var res = convert<whitelistDTO>.convertDBsetToT(dataSet);
            return res;
        }

        // POST: api/WhitList
        /// <summary>
        /// This function adds an ip to the list in the db.
        /// </summary>
        /// <param name="whiteListDTO"></param>
        /// <returns>the ip that inserted or empty list if the requested ip already exist.</returns>
        [HttpPost]
        [Route("{action}")]

        public List<whitelistDTO> AddIP([FromBody] List<whitelistDTO> whitelistDTO)
        {
            List<whitelistDTO> listnull = new List<whitelistDTO>();
            List<SqlParameter> list = convert<whitelistDTO>.convertTypeToSqlParameter(whitelistDTO);
            Idal.IwhiteList whitrlist = factory.factorycs.resolve<IwhiteList>();
            var checkExist=whitrlist.IsIPExist(connectionString.connect, list);
            var convert1 = convert<whitelistDTO>.convertDBsetToT(checkExist);
            Ibll.IwhiteListBll whiteListBll = factory.factorycs.resolve<IwhiteListBll>();
            if (!whiteListBll.CheckExist(convert1))//it means that the ip does not exist already.
            {
                var db = whitrlist.AddIp(connectionString.connect, list);
                var res = convert<whitelistDTO>.convertDBsetToT(db);
                return res;
            }
            else //the ip already exist, so don't add it again.
            {
                return listnull;
            }           
        }

        // PUT: api/WhitList/5
        /// <summary>
        /// Function fot editing the description of ip address
        /// </summary>
        /// <param name="whitelistDTO"></param>
        /// <returns>The updated ip address</returns>
        [HttpPost]
        [Route("{action}")]

        public List<whitelistDTO> EditDescription([FromBody] List<whitelistDTO> whitelistDTO)
        {
            List<SqlParameter> list =convert<whitelistDTO>.convertTypeToSqlParameter(whitelistDTO);
            Idal.IwhiteList whitrlist = factory.factorycs.resolve<IwhiteList>();
            var db = whitrlist.EditDescription(connectionString.connect, list);
            var res = convert<whitelistDTO>.convertDBsetToT(db);
            return res;
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Function for deleting ip address.
        /// </summary>
        /// <param name="whitelist"></param>
        /// <returns>A number if succseeded, and 0 if failed.</returns>
        [HttpPost]
        [Route("{action}")]

        public List<int> DeleteIp([FromBody] List< whitelistDTO> whitelist)
        {
            List<SqlParameter> list = convert<whitelistDTO>.convertTypeToSqlParameter(whitelist);
            Idal.IwhiteList whitrlist = factory.factorycs.resolve<IwhiteList>();
            var db = whitrlist.DeleteIp(connectionString.connect, list);
            var res = convert<int>.convertDBsetToT(db);
            return res;
        }
    }
}

﻿using System.Collections.Generic;
using cenvert;
using DTO;
using Idal;
using Microsoft.AspNetCore.Mvc;
using BackOffice;

namespace backOfficeApi.Controllers
{
    /// <summary>
    /// This controller handeled the failed list actions 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class failedController : ControllerBase
    {
        // GET: api/failed
        /// <summary>
        /// gets all the failed ip's from the database.
        /// </summary>
        /// <returns>return all the failed ip's from the database.</summary>
        [HttpGet]
        [Route("{action}")]
        public List<failedDTO> GetAllFailed()
        {
            Idal.Ifailed failed = factory.factorycs.resolve<Ifailed>();
            var db = failed.GetAllFailed(connectionString.connect);
            return convert<failedDTO>.convertDBsetToT(db);
        }
        /// <summary>
        /// Add an ip address to the failed table.
        /// </summary>
        /// <param name="failedList"></param>
        /// <returns>the requested ip if exist in the white list,else return an empty list</returns>
        [HttpPost]
        [Route("{action}")]
        public List<failedDTO> AddFailed(List<failedDTO> failedList)
        {
            Idal.Ifailed failed = factory.factorycs.resolve<Ifailed>();
            var convertedToSql = convert<failedDTO>.convertTypeToSqlParameter(failedList);
            var db = failed.AddFailedIp(connectionString.connect, convertedToSql);
            return convert<failedDTO>.convertDBsetToT(db);
        }
    }
}

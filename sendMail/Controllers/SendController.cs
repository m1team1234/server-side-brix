﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using cenvert;
using DTO;
using Ibll;
using Idal;
using Microsoft.AspNetCore.Mvc;
using MailMessage;

namespace sendMail.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendController : ControllerBase
    {
        [HttpPost]
        [Route("{action}")]

        public SendResponse send([FromBody] SendRequest MailMessageDTO)
        {
            var SendResponse = new SendResponse();
            var ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList.GetValue(1).ToString();
            Ibll.Imailbll mailBll = factory.factorycs.resolve<Imailbll>();
            var ipDTO = new List<whitelistDTO>() { new whitelistDTO() { userIp = ip } };
            List<SqlParameter> list = convert<whitelistDTO>.convertTypeToSqlParameter(ipDTO);
            IwhiteList whiteList = factory.factorycs.resolve<IwhiteList>();
            var db = whiteList.IsIPExist(connectionString.connect, list);
            var convert2 = convert<whitelistDTO>.convertDBsetToT(db);
            IwhiteListBll whiteListBll = factory.factorycs.resolve<IwhiteListBll>();
            if (whiteListBll.CheckExist(convert2))
            {
                var convert3 = convertMeilMessage.convertDTOToMailMessage(MailMessageDTO);
                mailBll.Send(convert3);
                SendResponse.IsSent = true;
                return SendResponse;
            }
            else
            {
                SendResponse.IsSent = false;
                return SendResponse;
            }

        }
    }
}
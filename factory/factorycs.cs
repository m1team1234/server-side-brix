﻿
using Idal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace factory
{
    public class factorycs
    {
        private static factorycs instace;
        private static Type[] mytypes { get; set; }
        private static Implement myAttributes { get; set; }
        private static Dictionary<Type, Type> FindClass { get; set; }
        private static List<string> foldersList { get; set; }
        private void LoadFile()
        {
            foldersList = new List<string>();
            var execPath = AppDomain.CurrentDomain.BaseDirectory;
            foldersList.Add(execPath + "/dlls");
            FindClass = new Dictionary<Type, Type>();
            foreach (var folder in foldersList)
            {
                var arrFiles = Directory.GetFiles(folder, "*.dll");
                foreach (var assembly in arrFiles)
                {
                    Assembly asm = Assembly.LoadFrom(assembly);
                    mytypes = asm.GetTypes();
                    foreach (var type in mytypes)
                    {
                        if (type.IsClass)
                        {
                            myAttributes = type.GetCustomAttribute<Implement>();
                        }
                        if (myAttributes != null)
                        {
                            FindClass.Add(myAttributes.Implementaion, type);

                        }

                    }
                }
            }

        }
        public static T resolve<T>() where T : class
        {
            //FindClass is the dictionary that contains the interfaces and their classes.
            var result = FindClass[typeof(T)];
            if (result != null)
            {
                return Activator.CreateInstance(FindClass[typeof(T)]) as T;
            }
            else
            {
                throw new Exception("No class was found to implement the requested interface");
            }

        }
        private factorycs()
        {
            LoadFile();
        }
        public static factorycs GetInstace()
        {
            if (instace == null)
                instace = new factorycs();
            return instace;
        }
    }
}

  
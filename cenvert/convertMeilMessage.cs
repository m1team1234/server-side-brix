﻿using DTO;
using MailMessage;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace cenvert
{
    public class convertMeilMessage
    {
        public static System.Net.Mail.MailMessage convertDTOToMailMessage(SendRequest MailMessageDTO)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.Body = MailMessageDTO.body;
            message.Subject = MailMessageDTO.subject;
            message.From = new MailAddress(MailMessageDTO.from);
            foreach (var item in MailMessageDTO.listTo)
            {               
                message.To.Add(item);
            }
            if (MailMessageDTO.listAttchment != null)
            {
                foreach (var item in MailMessageDTO.listAttchment)
                {
                    message.Attachments.Add(new Attachment(item));
                }
            } 
            return message;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Idal
{
   public interface Ifailed
    {
        DataSet GetAllFailed(string connectionString);
        DataSet AddFailedIp(string connectionString, List<SqlParameter> list);
        DataSet DeleteFailedIp(string connectionString, List<SqlParameter> list);
        DataSet IsfailedIpIPExist(string connectionString, List<SqlParameter> list);
    }
}

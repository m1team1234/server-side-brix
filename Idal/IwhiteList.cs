﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Idal
{
   public interface IwhiteList
    {
        DataSet GetAllList(string connectionString);
        DataSet AddIp(string connectionString, List<SqlParameter> list);
        DataSet DeleteIp(string connectionString, List<SqlParameter> list);
        DataSet IsIPExist(string connectionString, List<SqlParameter> list);
        DataSet EditDescription(string connectionString, List<SqlParameter> list);
    }
}

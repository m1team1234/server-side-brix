### Brix project!!!###

To use this repository, you should folow some steps:

1. Clone the server side brix repository to an empty folder.
2. Execute the script.sql file in sql server management.
3. open the back office folder, There is the main solution and set the BackOffice project as startUp project.
4. Change the connection string in the appsetting of the back office project and of the sendMail project.
5. Go to https://bitbucket.org/m1team1234/client-brix/src/master and clone it,then you would be able to see how it's work together.

### Send email###
1. set the sendMail project as startUp project in the backOffice solution.
2. Put your Ip address in the whiteList table in the db, you can do it from the backOffice client.
3. Create a client project to use sendMail, install CRB.MailClient.Contracts package from Nuget.org
4. Another option is to open CheckSendMail, It's another solution so that it's possible to run them both together and see how it work.

### Extensive explanation###
 Extensive explanation about the architecture of the layers in our projec:
 

In our project we separated the code into a few layers and auxiliary components.
##### The advantages of the separation are:#####
*	Maintenance - It is much easier and simple to maintain small software components than one large software component
*	Scalability - Any software component can grow or change independently of the other components
*	Flexibility - You can replace / change a component in the system without affecting the other components
*	Teamwork - Separating the project to various teams, professionalizing software professionals in a specific part of the system.
*	Reuse - We can use components written to one system on other systems as well.
Now we’ll explain each layer or auxiliary component:
#### The Data Access Layer- ####
In this layer we manage the connection to the database.
We create a function that invoke procedures in the db. The function gets a procedure name, and parameters - optionally.
#### The Data Transfer Object auxiliary component:####
In this component we create classes that match to the client side needs.
This classes contains only properties so that the transmission will be light and fast.
In order to convert to DTO classes to the needed type we built a auxiliary component  - Conversion.
#### Conversion auxiliary component:####
In this component we created functions to convert DTO to SQLParameters  and DBSet to DTO.
This component is written by reflection so that it works for all.
#### The Business Logic Layer:####
In this layer we check the data that accepted from the db.
#### Interfaces auxiliary component:####
In this component we created a parallel interfaces for all class in the DAL layer and the BLL layer.
#### Factory auxiliary component:####
This component built a dictionary by reflection that contains interfaces and the types that implement them.
#### Web.Api Layer:####
This layer contains the controllers that open to the client side.

After the client side and server side are done we publish them and generated the deployment.

### Deployment###
We wrote a batch file that generate the site in the IIS Manager.
The batch file is generic and suitable for all projects.